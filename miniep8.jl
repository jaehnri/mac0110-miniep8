# MAC0110 - MiniEP8
# João Henri Carrenho Rocha - 11796378

using Test

const diamonds = 1
const spades   = 2
const hearts   = 3
const clubs    = 4

const J = 11
const Q = 12
const K = 13
const A = 14 

function switch(v, i, j)
    aux = v[i]
    v[i] = v[j]
    v[j] = aux
end

function insercao(v)
    tam = length(v)

    for i in 2:tam
        j = i
    
            while j > 1
                if compareByValueAndSuit(v[j], v[j - 1]) 
                    switch(v, j, j - 1)
                else
                    break
                end
                j = j - 1
            end
    end

    return v
end

function changeLetterForConstant(cardValue)
    if cardValue == "J"
        return J
    elseif cardValue == "Q"
        return Q
    elseif cardValue == "K"
        return K
    elseif cardValue == "A"
        return A
    else
        return parse(Int64, cardValue)
    end
end

function getValue(card)
    return card[1:lastindex(card)-1]
end

function compareByValue(x, y)
    return changeLetterForConstant(getValue(y)) > changeLetterForConstant(getValue(x))
end

function getSuit(card) 
    if occursin("♦", card)
        return diamonds
        
    elseif occursin("♠", card)
        return spades

    elseif occursin("♥", card)
        return hearts

    else
        return clubs
    end
end

function compareByValueAndSuit(x, y)
    if getSuit(y) > getSuit(x)
        return true
    elseif getSuit(x) > getSuit(y)
        return false
    else
        return compareByValue(x, y)
    end
end

@testset "compare by value" begin
    @test compareByValue("2♠", "A♠")
    @test compareByValue("K♥", "10♥") == false
    @test compareByValue("10♠", "10♥") == false
    @test compareByValue("10♠", "10♠") == false
end

@testset "compare by value and suit" begin
    @test compareByValueAndSuit("2♠", "A♠")
    @test compareByValueAndSuit("10♠", "10♥")
    @test compareByValueAndSuit("A♠", "2♥")
    @test compareByValueAndSuit("K♥", "10♥") == false
end

@testset "check insertion sort" begin
    @test insercao(["10♥", "10♦", "K♠", "A♠", "J♠", "A♠"]) == ["10♦", "J♠", "K♠", "A♠", "A♠", "10♥"]
    @test insercao(["10♥", "10♥", "10♥", "10♥", "10♥", "A♠"]) == ["A♠", "10♥", "10♥", "10♥", "10♥", "10♥"]
    @test insercao(["10♦", "10♠", "10♥", "10♣"]) == ["10♦", "10♠", "10♥", "10♣"]
    @test insercao(["10♠", "10♣", "10♥", "10♦"]) == ["10♦", "10♠", "10♥", "10♣"]
    @test insercao(["A♠", "J♠", "2♠", "7♠"]) == ["2♠", "7♠", "J♠", "A♠"]
end